package com.serumula.eazymoola.persistance.jsonSeria;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

public class JsonUtil {

    static ObjectMapper objectMapper = new ObjectMapper();

    public static <T> String serializeObject(T object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

    public static <T> T DeSerializeString(String object, Class<T> tClass) throws IOException {
        return (T) new ObjectMapper().readValue(new StringReader(object), tClass);

    }

    public static <T> T DeSerializeObject(String object, Class<T> tClass) throws IOException {
        return (T) objectMapper.readValue(object, tClass.getClass());
    }

    public static <T> T DeSerializeObjectList(String object, Class<T> tClass) throws IOException {

        return (T) objectMapper.readValue(object, new TypeReference<List<T>>() {
        });

    }

    public static <T> List<T> DeSerializeObjectList(String object, TypeReference<List<T>> listTypeReference) throws IOException {

        return (List<T>) objectMapper.readValue(object, listTypeReference);

    }
}
