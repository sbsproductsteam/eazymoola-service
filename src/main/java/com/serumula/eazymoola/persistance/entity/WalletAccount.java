package com.serumula.eazymoola.persistance.entity;

import com.serumula.core.service.persistance.enums.WalletAccountStatus;
import lombok.*;
import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Table(name = "WalletAccount")
@Entity
@EqualsAndHashCode(doNotUseGetters = true)
@Builder
public class WalletAccount implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "WALLET_ACCOUNT_ID")
    @Setter(AccessLevel.NONE)
    private Long WalletAccountId;

    @Setter(AccessLevel.NONE)
    @Column(name = "WALLET_REFERENCE_NUMBER", unique = true, nullable = false)
    private String userWalletReferenceNumber;

    @Setter(AccessLevel.NONE)
    private Date createdTimeStamp;

    @Setter(AccessLevel.NONE)
    private Date updatedTimeStamp ;

    @Enumerated(EnumType.STRING)
    private WalletAccountStatus accountStatus;

    //TODO extract user later
    //owner details
    private String userName;

    @Column(name = "MSISDN", unique = true, nullable = false)
    private String msisdn;

    //TODO extract balance later

    private double WalletAccountBalance;

    @PrePersist
    private void genReferences(){
//        Long.toHexString(Double.doubleToLongBits(Math.random()));
//        UUID.randomUUID().toString();
        this.userWalletReferenceNumber = RandomStringUtils.randomAlphanumeric(6);
        this.createdTimeStamp = new Date();
    }

    @PreUpdate
    private void updateFields(){
        this.updatedTimeStamp = new Date();
    }
}
