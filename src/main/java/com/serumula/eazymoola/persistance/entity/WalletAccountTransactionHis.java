package com.serumula.eazymoola.persistance.entity;//package com.serumula.core.service.persistance.entity;
//
//import lombok.*;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.Date;
//
//@NoArgsConstructor
//@AllArgsConstructor
//@ToString
//@Data
//@Table(name = "WALLET_ACCOUNT_TRANSACTION_HIS")
//@Entity
//@EqualsAndHashCode(doNotUseGetters = true)
//@Builder
//public class WalletAccountTransactionHis implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//
//    @Column(name = "WALLET_ACCOUNT_TRANSACTION_ID")
//    private Long WalletAccountTransactionId;
//
//    @Setter(AccessLevel.NONE)
//    @Column(name = "ENTRY_DATE")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date entryDate;
//
//    private double entryAmount;
//
//    @Enumerated(EnumType.STRING)
//    private String entryType;
//
//    private double WalletAccountBalance;
//}
