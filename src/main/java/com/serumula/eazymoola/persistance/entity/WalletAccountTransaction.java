package com.serumula.eazymoola.persistance.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
@Table(name = "WALLET_ACCOUNT_TRANSACTION")
@Entity
@EqualsAndHashCode(doNotUseGetters = true)
@Builder
public class WalletAccountTransaction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "WALLET_ACCOUNT_TRANSACTION_ID")
    @Setter(AccessLevel.NONE)
    private Long walletAccountTransactionId;

    private String transactionDescription;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "WALLET_ACCOUNT", referencedColumnName = "WALLET_ACCOUNT_ID", nullable = false)
    private WalletAccount walletAccount;

    @Setter(AccessLevel.NONE)
    @Column(name = "ENTRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date entryDate;

    private double entryAmount;

    @Enumerated(EnumType.STRING)
    private EntryType entryType;

    private double walletAccountBalance;



    @PrePersist
    private void updateFields(){
        this.entryDate = new Date();
    }

    public enum EntryType {
        IN_COMING("In Coming"),
        OUT_GOING("Out Coming");
        private final String description;

        EntryType(String description) {
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }

    }
}
