package com.serumula.eazymoola.persistance.enums;

public enum WalletAccountStatus {
    ACTIVE("Active"),
    SUSPENDED("Suspended"),
    CLOSED("Closed"),
    HALT("Halt");

    private final String description;

    WalletAccountStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

}