package com.serumula.eazymoola.persistance.repository;

import com.serumula.core.service.persistance.entity.WalletAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WalletAccountRepository extends CrudRepository<WalletAccount, Long> {

    Optional<WalletAccount> findByUserWalletReferenceNumber(final String userWalletReferenceNumber);

    Optional<WalletAccount> findByMsisdn(final String msisdn);

}
