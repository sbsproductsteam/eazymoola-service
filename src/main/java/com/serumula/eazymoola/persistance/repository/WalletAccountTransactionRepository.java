package com.serumula.eazymoola.persistance.repository;

import com.serumula.core.service.persistance.entity.WalletAccountTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletAccountTransactionRepository
        extends CrudRepository<WalletAccountTransaction, Long> {


}
