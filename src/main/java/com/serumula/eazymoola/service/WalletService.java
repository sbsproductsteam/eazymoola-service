package com.serumula.eazymoola.service;

import com.serumula.core.service.api.dto.ResponseStatusDto;
import com.serumula.core.service.api.dto.wallet.WalletPayOutDto;
import com.serumula.core.service.api.dto.wallet.WalletTopUpDto;
import com.serumula.core.service.exception.InvalidInputException;
import com.serumula.core.service.exception.RecordCreationFailedException;
import com.serumula.core.service.exception.RecordNotFoundException;

public interface WalletService {

    <T extends ResponseStatusDto> T walletDeposit(String userRefNumber, WalletTopUpDto requestDto) throws
            RecordCreationFailedException, InvalidInputException;

    <T extends ResponseStatusDto> T getUserWalletStatement(String userRefNumber) throws
            RecordNotFoundException, InvalidInputException;

    <T extends ResponseStatusDto> T payOut(String userRefNumber, WalletPayOutDto requestDto) throws
            RecordCreationFailedException;

    <T extends ResponseStatusDto> T getUserWalletAccount(String msisdn, String userName) throws
            RecordNotFoundException, InvalidInputException;
}
