package com.serumula.eazymoola.service;

import com.serumula.core.service.api.dto.ResponseStatusDto;
import com.serumula.core.service.api.dto.wallet.WalletAccountDto;
import com.serumula.core.service.api.dto.wallet.WalletPayOutDto;
import com.serumula.core.service.api.dto.wallet.WalletTopUpDto;
import com.serumula.core.service.exception.InvalidInputException;
import com.serumula.core.service.exception.RecordCreationFailedException;
import com.serumula.core.service.exception.RecordNotFoundException;
import com.serumula.core.service.persistance.entity.FeeCategoryUserAllocation;
import com.serumula.core.service.persistance.entity.FeeCategoryUserAllocationId;
import com.serumula.core.service.persistance.entity.WalletAccount;
import com.serumula.core.service.persistance.entity.WalletAccountTransaction;
import com.serumula.core.service.persistance.enums.WalletAccountStatus;
import com.serumula.core.service.persistance.repository.FeeCategoryUserAllocationRepository;
import com.serumula.core.service.persistance.repository.WalletAccountRepository;
import com.serumula.core.service.persistance.repository.WalletAccountTransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Profile("default")
@Service
@Slf4j
public class WalletServiceImpl implements WalletService {
    private WalletAccountRepository walletAccountRepository;
    private WalletAccountTransactionRepository walletAccountTransactionRepository;
    private FeeCategoryUserAllocationRepository feeCategoryUserAllocationRepository;


    @Autowired
    public WalletServiceImpl(
            WalletAccountRepository walletAccountRepository,
            WalletAccountTransactionRepository walletAccountTransactionRepository,
            FeeCategoryUserAllocationRepository feeCategoryUserAllocationRepository
    ) {
        this.walletAccountRepository = walletAccountRepository;
        this.walletAccountTransactionRepository = walletAccountTransactionRepository;
        this.feeCategoryUserAllocationRepository = feeCategoryUserAllocationRepository;
    }

    @Override
    public <T extends ResponseStatusDto> T getUserWalletStatement(String userRefNumber)
            throws RecordNotFoundException, InvalidInputException {
        return null;
    }

    @Override
    public <T extends ResponseStatusDto> T walletDeposit(String userRefNumber, WalletTopUpDto requestDto)
            throws RecordCreationFailedException, InvalidInputException {
        WalletAccount walletAccount = walletAccountRepository.findByUserWalletReferenceNumber(userRefNumber)
                .orElseThrow(() -> new InvalidInputException("Account with the user ref not found : User ref ->  " + userRefNumber));
        walletAccount.setWalletAccountBalance(walletAccount.getWalletAccountBalance()
                + requestDto.getTransactionAmount());
        try {
            walletAccountTransactionRepository.save(WalletAccountTransaction.builder()
                    .entryAmount(requestDto.getTransactionAmount())
                    .walletAccount(walletAccount)
                    .walletAccountBalance(walletAccount.getWalletAccountBalance())
                    .transactionDescription(requestDto.getDescription())
                    .entryType(WalletAccountTransaction.EntryType.IN_COMING)
                    .build());
        }catch (Exception e){
            e.printStackTrace();
            throw new RecordCreationFailedException ("Failed to update Wallet Transactions -> " + e.getMessage());
        }
        return (T) (ResponseStatusDto) () -> "Query executed successfully";
    }

    @Override
    @Transactional
    public <T extends ResponseStatusDto> T payOut(String userRefNumber, WalletPayOutDto requestDto)
            throws RecordCreationFailedException {

        WalletAccount walletAccount = walletAccountRepository.findByUserWalletReferenceNumber(userRefNumber)
                .orElseThrow(() -> new RecordCreationFailedException("User Account [ " + StringUtils.capitalize(userRefNumber )+ " ] does not exist"));


        if (walletAccount.getWalletAccountBalance() < 0.0) {
            throw new RecordCreationFailedException("User Account [ " + StringUtils.capitalize(userRefNumber) + " ] is overdrawn");
        }

        for (WalletPayOutDto.PayoutItem payoutItem : requestDto.getPayoutItems()) {
            if (walletAccount.getWalletAccountBalance() - payoutItem.getPaymentAmount() < 0) {
                throw new RecordCreationFailedException("User Account [ " + StringUtils.capitalize(userRefNumber )+ " ] has insufficient amount");
            }

            FeeCategoryUserAllocationId id = new FeeCategoryUserAllocationId(payoutItem.getUserFeeItemIdentity().getYear(),
                    payoutItem.getUserFeeItemIdentity().getSchoolId(), payoutItem.getUserFeeItemIdentity().getFeeCategoryId(), payoutItem.getUserFeeItemIdentity().getUserId());

            final FeeCategoryUserAllocation feeCategoryUserAllocation = feeCategoryUserAllocationRepository.findById(id).orElseThrow(
                    () -> new RecordCreationFailedException("Item to be paid for, does not exist on the provider shelve: Item Name -> " + payoutItem.getItemDescription()));

            // payout
            feeCategoryUserAllocation.setPaidAmount(feeCategoryUserAllocation.getPaidAmount()+payoutItem.getPaymentAmount());
            feeCategoryUserAllocation.setBalanceAmount(feeCategoryUserAllocation.getBalanceAmount() - payoutItem.getPaymentAmount());
            if (feeCategoryUserAllocation.getOverDueAmount() > 0.0){
                if (feeCategoryUserAllocation.getOverDueAmount() <= payoutItem.getPaymentAmount()){
                    feeCategoryUserAllocation.setOverDueAmount(0.0);
                    feeCategoryUserAllocation.setDueAmount(feeCategoryUserAllocation.getDueAmount()
                            - (payoutItem.getPaymentAmount()-feeCategoryUserAllocation.getOverDueAmount()));
                }
                else{
                    feeCategoryUserAllocation.setOverDueAmount(feeCategoryUserAllocation.getOverDueAmount() - payoutItem.getPaymentAmount());
                }

            }
            else{
                feeCategoryUserAllocation.setDueAmount(feeCategoryUserAllocation.getDueAmount() - payoutItem.getPaymentAmount());
            }
            feeCategoryUserAllocationRepository.save(feeCategoryUserAllocation);

            walletAccount.setWalletAccountBalance(walletAccount.getWalletAccountBalance() - payoutItem.getPaymentAmount());

            walletAccountRepository.save(walletAccount);

            walletAccountTransactionRepository.save(WalletAccountTransaction.builder()
                    .entryType(WalletAccountTransaction.EntryType.OUT_GOING)
                    .walletAccount(walletAccount)
                    .transactionDescription(payoutItem.getItemDescription())
                    .entryAmount(payoutItem.getPaymentAmount())
                    .walletAccountBalance(walletAccount.getWalletAccountBalance())
                    .build());
        }

        return (T) (ResponseStatusDto) () -> "Query executed successfully";
    }

    @Override
    public <T extends ResponseStatusDto> T getUserWalletAccount(String msisdn, String userName)
            throws RecordNotFoundException, InvalidInputException {
        WalletAccount walletAccount = walletAccountRepository.findByMsisdn(msisdn).orElse(new WalletAccount());

        if (!StringUtils.isEmpty(walletAccount.getUserWalletReferenceNumber())) {
            if (walletAccount.getAccountStatus() != WalletAccountStatus.ACTIVE)
                throw new InvalidInputException("User Account [ " + msisdn + " ] exists but its not active ");
        } else {
            walletAccount.setWalletAccountBalance(0.0);
            walletAccount.setUserName(userName);
            walletAccount.setMsisdn(msisdn);
            walletAccount.setAccountStatus(WalletAccountStatus.ACTIVE);
            walletAccountRepository.save(walletAccount);
        }


        return (T) new ResponseStatusDto() {
            @Override
            public String getMessage() {
                return "Query executed successfully";
            }

            public WalletAccountDto getWalletAccountDto() {
                return WalletAccountDto.builder()
                        .msisdn(walletAccount.getMsisdn())
                        .userName(walletAccount.getUserName())
                        .userWalletReferenceNumber(walletAccount.getUserWalletReferenceNumber())
                        .walletAccountId(walletAccount.getWalletAccountId())
                        .balance(walletAccount.getWalletAccountBalance())
                        .build();
            }
        };
    }
}
