package com.serumula.eazymoola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EazymoolaMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(EazymoolaMainApplication.class, args);
    }

}
