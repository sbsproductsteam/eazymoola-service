package com.serumula.eazymoola.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.serumula.core.service.api.dto.ResponseStatusDto;
import com.serumula.core.service.api.dto.wallet.WalletPayOutDto;
import com.serumula.core.service.api.dto.wallet.WalletTopUpDto;
import com.serumula.core.service.exception.InvalidInputException;
import com.serumula.core.service.exception.RecordCreationFailedException;
import com.serumula.core.service.exception.RecordNotFoundException;
import com.serumula.core.service.service.WalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api")
@Api(value = "School Transactional Resource REST Endpoint", description = "Shows the E-School back-end APIs", tags = "E-School Management APIs")
@Validated
@Slf4j
public class WalletResource {

    WalletService walletService;

    @Autowired
    private WalletResource(
            WalletService walletService
    ) {
        this.walletService =walletService;

    }

    @ApiOperation(value = "Deposits into user wallet ", response = ResponseStatusDto.class
            , tags = "Wallet")
    @PostMapping(path = "/deposit-into-user-wallet/{user-ref-number}")
    public ResponseStatusDto walletCreateAndDeposit( @PathVariable("user-ref-number") String userRefNumber,
                                                        @RequestBody WalletTopUpDto requestDto)
            throws RecordCreationFailedException, InvalidInputException {
        log.debug("Updating the Wallet Balances " + requestDto);
        return walletService.walletDeposit(userRefNumber, requestDto);
    }

    @ApiOperation(value = "Pays from the wallet ", response = ResponseStatusDto.class
            , tags = "Wallet")
    @PostMapping(path = "/pay-by-user-wallet/{user-ref-number}")
    public ResponseStatusDto payoutItemByUserWallet( @PathVariable("user-ref-number") String userRefNumber,
                                                        @RequestBody WalletPayOutDto requestDto)
            throws RecordCreationFailedException, JsonProcessingException {
        log.debug("Updating the Wallet Balances " + requestDto);
        return walletService.payOut(userRefNumber, requestDto);
    }


    @ApiOperation(value = "get user wallet statement", response = ResponseStatusDto.class
            , tags = "Wallet")
    @GetMapping(path = "/get-user-statement/{user-ref-number}")
    public ResponseStatusDto getUserWalletStatement(@PathVariable("user-ref-number") String userRefNumber)
            throws RecordNotFoundException, InvalidInputException {
        log.info("getting the user wallet statement -> {}", userRefNumber);
        return  walletService.getUserWalletStatement(userRefNumber);
    }

    @ApiOperation(value = "get user wallet account", response = ResponseStatusDto.class
            , tags = "Wallet")
    @GetMapping(path = "/get-user-wallet-account/msisdn/{msisdn}/user-name/{user-name}")
    public ResponseStatusDto getUserWalletAccount(
            @PathVariable(value = "msisdn",required = false) String msisdn,
            @PathVariable(value = "user-name", required = false) String userName)
            throws RecordNotFoundException, InvalidInputException {
        log.info("getting the user wallet account -> user name {} > MSISDN  {}", userName, msisdn);
        return  walletService.getUserWalletAccount(msisdn, userName);
    }
}
