package com.serumula.eazymoola.api.dto.wallet;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Builder
@ToString(doNotUseGetters = true)
public class WalletStatementDto {
    private Date statementDate;

    private String firstName;
    private String lastName;
    private String userRefNumber;

    private List<StatementDetail> statementDetails = new ArrayList<>();


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @SuperBuilder
    @Builder
    @ToString(doNotUseGetters = true)
    public static class StatementDetail {
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private LocalDate transactionDate;
        private double transactionAmount;
        private String description;
        private double balanceAmount;

        protected TransactionType paymentMethod;

        public static enum TransactionType {
            IN("In Coming Money"),
            OUT("Out Going Money");

            private final String description;

            TransactionType(String description) {
                this.description = description;
            }

            public String getDescription() {
                return this.description;
            }
        }
    }
}
