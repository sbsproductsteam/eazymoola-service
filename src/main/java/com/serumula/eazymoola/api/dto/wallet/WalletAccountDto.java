package com.serumula.eazymoola.api.dto.wallet;


import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Builder
@ToString(doNotUseGetters = true)
public class WalletAccountDto {
        private Long walletAccountId;
        private String userWalletReferenceNumber;
        private String userName;
        private String msisdn;
        private double balance;
}
