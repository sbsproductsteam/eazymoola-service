package com.serumula.eazymoola.api.dto.wallet;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Builder
@ToString(doNotUseGetters = true)
public class WalletPayOutDto {
    private List<PayoutItem> payoutItems = new ArrayList<>();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @SuperBuilder
    @Builder
    @ToString(doNotUseGetters = true)
    public static class PayoutItem {
        private String itemDescription;
        private double paymentAmount;

        private UserFeeItemIdentity  userFeeItemIdentity;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @SuperBuilder
        @Builder
        @ToString(doNotUseGetters = true)
        public static class UserFeeItemIdentity {
            private Long year;
            private Long schoolId;
            private Long feeCategoryId;
            private Long userId;

        }

    }
}
