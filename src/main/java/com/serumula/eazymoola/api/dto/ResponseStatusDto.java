package com.serumula.eazymoola.api.dto;

public interface ResponseStatusDto {
  String getMessage();
}
