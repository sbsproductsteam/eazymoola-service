package com.serumula.eazymoola.api.dto.wallet;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Builder
@ToString(doNotUseGetters = true)
public class WalletTopUpDto {
    private double transactionAmount;
    private String description;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private WalletAccountDto walletAccount;
}
