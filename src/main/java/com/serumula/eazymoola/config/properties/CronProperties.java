package com.serumula.eazymoola.config.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "cron")
@Setter
@Getter
@ToString
public class CronProperties {
    private String expression;
    private String fixedRateInMilliseconds;
    private String delayRateInMilliseconds;
}
